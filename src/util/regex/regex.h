#ifndef __REGEX_H__
#define __REGEX_H__

#include <string.h>
#include <regex.h>


void regex_getRegexString(char *buffero, char *bufferi, regmatch_t *pmatch, int numMatch);

#endif
