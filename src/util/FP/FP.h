#ifndef __FP_H__
#define __FP_H__

#define lambda(return_type, function_body) \
({ \
      return_type __fn__ function_body \
          __fn__; \
})

#endif
