#include "./endian.h"

uint8_t* swap(uint8_t *bytes, int size);

char getbit(char *bytes, unsigned int index);

uint8_t* swap(uint8_t *bytes, int size)
{
    int mid = (size)/2;
    int count;
    uint8_t tmp;
    
    count = 0;
    
    while(count < mid)
    {
        tmp = bytes[count];
        bytes[count] = bytes[size - count - 1];
        bytes[size - count - 1] = tmp;
        count++;
    }
    return bytes;
    
}

char getbit(char *bytes, unsigned int index)
{
    unsigned int pos = index / 8;
    unsigned int offset = index % 8;
    char pos_value = bytes[pos];
    return pos_value & (0x01 << offset);
}

