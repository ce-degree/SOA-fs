#ifndef __ENDIAN_H__
#define __ENDIAN_H__

#include <stdint.h>

uint8_t* swap(uint8_t *bytes, int size);

char getbit(char *bytes, unsigned int index);

#endif
