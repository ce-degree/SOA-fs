#include "./fs.h"

void fat16_write_clus(int fat_fd, fat16_t *fat16, uint8_t *block, uint32_t block_size, uint16_t cluster_num)
{

	ssize_t rwbytes;

	// Documentacion
	uint32_t root_dir_sectors = ((fat16->BPB_root_ent_cnt * 32) + (fat16->BPB_bytes_per_sec - 1)) / fat16->BPB_bytes_per_sec;
	uint32_t first_data_sector = fat16->BPB_rsvd_sec_cnt + (fat16->BPB_num_FATs * fat16->BPB_FATSz16) + root_dir_sectors;

	uint32_t first_sector_of_cluster = ((cluster_num - 2) * fat16->BPB_sec_per_clus) + first_data_sector;

	uint32_t offset_doc = first_sector_of_cluster * fat16->BPB_bytes_per_sec;

	//printf("first data address: %u\n", first_data_sector * fat16->BPB_bytes_per_sec);

	//printf("offset (doc): %u\n", offset_doc);

	// Move the file cursor to the desired offset
	lseek(fat_fd, offset_doc, SEEK_SET);

	// Write the ext2_block to the cluster
	rwbytes = write(fat_fd, block, block_size);

	if (rwbytes != (int)block_size) {
		printf("Ha escrito mal!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
	}

}

void fat16_create_entry_from_inode(fat16_t fat16, fat16_direntry_t *entry, ext2_inode_t inode, ext2_diren_t diren)
{

	regex_t regex;
	int reti;
	size_t nmatch;
	regmatch_t *pmatch;

	char file_name[256];
	uint8_t entry_name[12];
	char buff[512];
	int i;
	int size;

	if (entry == NULL) {
		exit(-1);
	}

	printf("Copying metadata.\n");

	// Copy the file name in a NULL-ended string
	memset(file_name, '\0', 256);
	strncpy(file_name, diren.name, 255);

	// Fill entry_name with blank spaces
	memset(entry_name, ' ', 11);
	entry_name[11] = '\0';

	// Extract the name

	// Check if it is a name with an extension
	reti = regcomp(&regex, "^(.+)\\.(.+)$", REG_EXTENDED | REG_ICASE);

	if (reti) {
		printf("Could not compile regex\n");
		exit(-1);
	}

	nmatch = 3;
	pmatch = malloc(sizeof(regmatch_t) * nmatch);

	reti = regexec(&regex, file_name, nmatch, pmatch, 0);

	if (!reti)
	{
		regfree(&regex);
		// File with extension
		regex_getRegexString(buff, file_name, pmatch, 1);

		for (i = 0; (i < 8) && buff[i] != '\0'; i++) {
			entry_name[i] = buff[i];
		}

		//printf("Entry name; %.*s\n", 8, entry_name);
		//getchar();

		regex_getRegexString(buff, file_name, pmatch, 2);

		for (i = 0; (i < 3) && buff[i] != '\0'; i++) {
			entry_name[i + 8] = buff[i];
		}

		//printf("Entry extension; %.*s\n", 8, entry_name);
		//getchar();

		free(pmatch);
	} else {
		free(pmatch);
		regfree(&regex);

		// File without extension
		for (i = 0; (i < 8) && file_name[i] != '\0'; i++) {
			entry_name[i] = file_name[i];
		}
	}


	strupper(entry_name);
	//printf("Entry name; %.*s\n", 11, entry_name);
	//getchar();
	
	fat16_direntry_response_t fileentries[512];

	fat16_searchfileentries(fat16, fileentries, &size);

	if (entry_name[0] == 0xE5) {
		entry_name[0] = 0x05;
	}

	for (i = 0; i < size; i++) {
		//printf("File with name: [%.*s] found; comparing with [%.*s]\n", 11, fileentries[i].entry.name, 11, entry_name);
		if (strncmp(fileentries[i].entry.name, entry_name, 11) == 0) {
			printf("A file with a FAT entry name: [%s] already exists, cannot copy.\n", entry_name);
			exit(-1);
		}
	}

	strncpy(entry->name, entry_name, 11);

	// Set ARCHIVE attribute
	entry->attr = FAT_ATTR_ARCHIVE;

	// Set file size
	entry->file_size = inode.i_size;

	// Set file time
	time_t now;
	struct tm *tnow;
	uint16_t fat_date = 0, fat_time = 0;

	now = time(NULL);

	tnow = localtime(&now);
	printf("Current date: %s", asctime(tnow));

	tnow->tm_year -= 80;
	tnow->tm_mon += 1;

	fat_date = ((tnow->tm_year & (0x7F)) << (0x9)) | ((tnow->tm_mon & (0xF)) << (0x5)) | (tnow->tm_mday & (0x1F));
	fat_time = ((tnow->tm_hour & (0x1F)) << (0xB)) | ((tnow->tm_min & (0x7E)) << (0x5)) | ((tnow->tm_sec / 2) & (0x1F));

	entry->crt_date = fat_date;
	entry->lst_acc_date = fat_date;
	entry->wrt_date = fat_date;

	entry->crt_time = fat_time;
	entry->crt_time_tenth = (tnow->tm_sec % 2) * 100;
	entry->wrt_time = fat_time;
	//entry->crt_time_tenth = 0x0;

}

int ext2_copy_inode_cb(void *arg, block_reader_context_t cntx)
{
	ext2_fat16_info_t *info = (ext2_fat16_info_t *)arg;
	if (cntx.block == 0) {
		info->fat_entry->fst_clus_lo = info->current_cluster;
		info->prev_cluster = info->current_cluster;
		//printf("Start block:%u\n", cntx.block);
		//printf("(Starting) Current cluster: %u\n", info->current_cluster);
	} else {
		info->prev_cluster = info->current_cluster;
		// Find the first free cluster
		info->current_cluster = fat16_next_free_cluster(info->fat16, info->prev_cluster + 1);
		//printf("Next free cluster: %u\n", info->current_cluster);
		info->fat16->FAT[info->prev_cluster] = info->current_cluster;
		//printf("fat16->FAT[%u] = %u\n", info->prev_cluster, info->fat16->FAT[info->prev_cluster]);

	}

	if (cntx.accum_read_size >= cntx.inode->i_size) {
		fat16_write_clus(info->fat_fd, info->fat16, cntx.block_content, cntx.block_size, info->current_cluster);
		info->fat16->FAT[info->current_cluster] = 0xFFFF;
		//printf("Writting FFFF in FAT[%u]\n", info->current_cluster);
		//printf("Finished\n");
		return 0;
	} else {
		//printf("Printing to cluster: %u\n", info->current_cluster);
		fat16_write_clus(info->fat_fd, info->fat16, cntx.block_content, cntx.block_size, info->current_cluster);
		//getchar();
		return 1;
	}
}

void ext2_copy_inode(int fd, ext2_t ext2, ext2_inode_t *inode, ext2_diren_t diren, int fat_fd, fat16_t *fat16, uint16_t cluster_num, fat16_direntry_t *fat_entry)
{

	cluster_num = cluster_num;

	ext2_fat16_info_t ef_info = {
		.ext2_fd = fd,
		.ext2 = ext2,
		.inode = inode,
		.inode_no = diren.inode,
		.fat_fd = fat_fd,
		.fat16 = fat16,
		.fat_entry = fat_entry,
		.prev_cluster = 0,
		.current_cluster = 0
	};

	fat16_create_entry_from_inode(*fat16, fat_entry, *inode, diren);

	// Find the first free cluster
	ef_info.current_cluster = fat16_next_free_cluster(fat16, ef_info.current_cluster);



	printf("Copying data.\n");

	if(inode->i_size == 0) {
		//printf("Current cluster: %u\n", ef_info.current_cluster);
		ef_info.fat_entry->fst_clus_lo = 0xFFFF;
		return;
	}

	ext2_block_reader(fd, ext2, diren.inode, inode, ext2_copy_inode_cb, &ef_info);
}

void strupper(char *s)
{
	unsigned int i;
	for (i = 0; i < strlen(s); i++) s[i] = toupper(s[i]);
}




