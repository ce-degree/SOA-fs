#include "./ext2.h"

uint32_t ext2_blockoffset(ext2_t ext2, uint32_t block);

uint32_t ext2_baseoffset(ext2_t ext2, uint32_t offset);

void ext2_print(ext2_t ext2);

void ext2_print_gdtables(ext2_t ext2);

void ext2_readfs(int fd, ext2_t *ext2);

int ext2_isext2(ext2_t ext2);

ext2_inodeloc_t ext2_find_inode(ext2_t ext2, unsigned int inode);

void ext2_read_inode(int fd, ext2_t ext2, int inode_no, ext2_inode_t *inode);

static void ext2_read_direns_for_file(int fd, ext2_t ext2, ext2_inode_t *inode, uint32_t inode_no, ext2_diren_t **diren, char *file);

int ext2_cat_inode(void *arg, block_reader_context_t cntx);

int ext2_search_file_entry(int fd, ext2_t ext2, char *file, ext2_diren_t **direns);

void ext2_free(ext2_t ext2);

int ext2_block_reader(int fd, ext2_t ext2, int inode_no, ext2_inode_t *inode, int (*callback)(void*, block_reader_context_t), void* arg);

uint32_t ext2_blockoffset(ext2_t ext2, uint32_t block)
{
	if (ext2.s_first_data_block == 1) {
		return (EXT2_BASEOFFSET(0) + (block - 1) * ext2.c_block_size);
	} else if (ext2.s_first_data_block == 0) {
		return (block) * ext2.c_block_size;
	}
	return 0;
}

/**
 * Dead function
 */
uint32_t ext2_baseoffset(ext2_t ext2, uint32_t offset)
{
	if (ext2.s_first_data_block == 1) {
		return 1024 + offset;
	}
	return 1024;
}

void ext2_print(ext2_t ext2)
{
	printf("Filesystem: EXT2");

	printf("\nINODE INFO\n");
	printf("Inode Size: %d\n", (int)ext2.s_indoe_size);
	printf("Number of Inodes: %d\n", (int)ext2.s_inodes_count);
	printf("First Inode: %d\n", (int)ext2.s_first_inode);
	printf("Inodes per group: %d\n", (int)ext2.s_inodes_per_group);
	printf("Free Inodes: %d\n", (int)ext2.s_free_inodes_count);

	printf("\nBLOCK INFO\n");
	printf("Block Size: %d\n", (int)(1024 << ext2.s_log_block_size));
	printf("Reserved Blocks: %d\n", (int)ext2.s_r_blocks_count);
	printf("Free Blocks: %d\n", (int)ext2.s_free_blocks_count);
	printf("Total Blocks: %d\n", (int)ext2.s_blocks_count);
	printf("First data block: %d\n", (int)ext2.s_first_data_block);
	printf("Blocks Per Group: %d\n", (int)ext2.s_blocks_per_group);
	printf("Frags Per Group: %d\n", (int)ext2.s_frags_per_group);

	printf("\nVOLUME INFO\n");
	printf("Volume Name: %s\n", (char *)ext2.s_volume_name);
	printf("Last Check: %s", asctime(localtime( (time_t *) & (ext2.s_lastcheck))));
	printf("Last Mounted: %s", asctime(localtime( (time_t *) & (ext2.s_mtime) )));
	printf("Last Written: %s", asctime(localtime( (time_t *) & (ext2.s_wtime) )));

}

void ext2_print_gdtables(ext2_t ext2)
{
	unsigned short count = 0;
	unsigned int n;

	while (count < ext2.gd_table.table_size) {

		printf("\nGD %d\n", count);

		printf("Block bitmap: %d\n", ext2.gd_table.table[count].bg_block_bitmap);
		printf("Inode bitmap: %d\n", ext2.gd_table.table[count].bg_inode_bitmap);
		printf("Inode Table bitmap: %d\n", ext2.gd_table.table[count].bg_inode_table);
		printf("Free Blocks: %d\n", ext2.gd_table.table[count].bg_free_blocks_count);
		printf("Free Inodes: %d\n", ext2.gd_table.table[count].bg_free_inodes_count);
		printf("Used dirs: %d\n", ext2.gd_table.table[count].bg_used_dirs_count);

		n = 0;
		printf("BLOCK BITMAP\n");
		while (n < ext2.s_blocks_per_group / 8) {
			printf("%2X ", ext2.gd_table.block_bitmap[n]);
			n++;
		}
		printf("\n");
		n = 0;
		printf("INODE BITMAP\n");
		while (n < ext2.s_inodes_per_group / 8) {
			printf("%2X ", ext2.gd_table.inode_bitmap[n]);
			n++;
		}
		printf("\n");

		count ++;

	}
}

void ext2_readfs(int fd, ext2_t *ext2)
{
	ssize_t rwbytes = 0;
	rwbytes = rwbytes;
	//unsigned int count;

	lseek(fd, EXT2_BASEOFFSET(56), SEEK_SET);
	rwbytes = read(fd, &ext2->s_magic, sizeof(ext2->s_magic));

	lseek(fd, EXT2_BASEOFFSET(88), SEEK_SET);
	rwbytes = read(fd, &ext2->s_indoe_size, sizeof(ext2->s_indoe_size));

	lseek(fd, EXT2_BASEOFFSET(0), SEEK_SET);
	rwbytes = read(fd, &ext2->s_inodes_count, sizeof(ext2->s_inodes_count));

	lseek(fd, EXT2_BASEOFFSET(84), SEEK_SET);
	rwbytes = read(fd, &ext2->s_first_inode, sizeof(ext2->s_first_inode));

	lseek(fd, EXT2_BASEOFFSET(40), SEEK_SET);
	rwbytes = read(fd, &ext2->s_inodes_per_group, sizeof(ext2->s_inodes_per_group));

	lseek(fd, EXT2_BASEOFFSET(16), SEEK_SET);
	rwbytes = read(fd, &ext2->s_free_inodes_count, sizeof(ext2->s_free_inodes_count));

	lseek(fd, EXT2_BASEOFFSET(24), SEEK_SET);
	rwbytes = read(fd, &ext2->s_log_block_size, sizeof(ext2->s_log_block_size));
	ext2->c_block_size = 1024 << ext2->s_log_block_size;

	lseek(fd, EXT2_BASEOFFSET(8), SEEK_SET);
	rwbytes = read(fd, &ext2->s_r_blocks_count, sizeof(ext2->s_r_blocks_count));

	lseek(fd, EXT2_BASEOFFSET(12), SEEK_SET);
	rwbytes = read(fd, &ext2->s_free_blocks_count, sizeof(ext2->s_free_blocks_count));

	lseek(fd, EXT2_BASEOFFSET(4), SEEK_SET);
	rwbytes = read(fd, &ext2->s_blocks_count, sizeof(ext2->s_blocks_count));

	lseek(fd, EXT2_BASEOFFSET(20), SEEK_SET);
	rwbytes = read(fd, &ext2->s_first_data_block, sizeof(ext2->s_first_data_block));

	lseek(fd, EXT2_BASEOFFSET(32), SEEK_SET);
	rwbytes = read(fd, &ext2->s_blocks_per_group, sizeof(ext2->s_blocks_per_group));

	lseek(fd, EXT2_BASEOFFSET(36), SEEK_SET);
	rwbytes = read(fd, &ext2->s_frags_per_group, sizeof(ext2->s_frags_per_group));

	lseek(fd, EXT2_BASEOFFSET(120), SEEK_SET);
	rwbytes = read(fd, &ext2->s_volume_name, sizeof(ext2->s_volume_name));

	lseek(fd, EXT2_BASEOFFSET(64), SEEK_SET);
	rwbytes = read(fd, &ext2->s_lastcheck, sizeof(ext2->s_lastcheck));
	ext2->s_lastcheck = ext2->s_lastcheck & 0xFFFFFFFF;

	lseek(fd, EXT2_BASEOFFSET(44), SEEK_SET);
	rwbytes = read(fd, &ext2->s_mtime, sizeof(ext2->s_mtime));
	ext2->s_mtime = ext2->s_mtime & 0xFFFFFFFF;

	lseek(fd, EXT2_BASEOFFSET(48), SEEK_SET);
	rwbytes = read(fd, &ext2->s_wtime, sizeof(ext2->s_wtime));
	ext2->s_wtime = ext2->s_wtime & 0xFFFFFFFF;

	// Read block group descriptor table
	unsigned short count;
	uint32_t offset;
	ext2->gd_table.table_size = ceil((float)ext2->s_blocks_count / (float)ext2->s_blocks_per_group);
	ext2->gd_table.table = malloc(sizeof(ext2_groupdesc_t) * ext2->gd_table.table_size);

	offset = ext2->s_first_data_block == 1 ? EXT2_BASEOFFSET(ext2->c_block_size) : ext2->c_block_size;
	count = 0;

	lseek(fd, offset, SEEK_SET);

	while (count < ext2->gd_table.table_size) {
		lseek(fd, offset + 0, SEEK_SET);
		rwbytes = read(fd, &(ext2->gd_table.table[count].bg_block_bitmap), sizeof(ext2->gd_table.table[count].bg_block_bitmap));

		lseek(fd, offset + 4, SEEK_SET);
		rwbytes = read(fd, &(ext2->gd_table.table[count].bg_inode_bitmap), sizeof(ext2->gd_table.table[count].bg_inode_bitmap));

		lseek(fd, offset + 8, SEEK_SET);
		rwbytes = read(fd, &(ext2->gd_table.table[count].bg_inode_table), sizeof(ext2->gd_table.table[count].bg_inode_table));

		lseek(fd, offset + 12, SEEK_SET);
		rwbytes = read(fd, &(ext2->gd_table.table[count].bg_free_blocks_count), sizeof(ext2->gd_table.table[count].bg_free_blocks_count));

		lseek(fd, offset + 14, SEEK_SET);
		rwbytes = read(fd, &(ext2->gd_table.table[count].bg_free_inodes_count), sizeof(ext2->gd_table.table[count].bg_free_inodes_count));

		lseek(fd, offset + 16, SEEK_SET);
		rwbytes = read(fd, &(ext2->gd_table.table[count].bg_used_dirs_count), sizeof(ext2->gd_table.table[count].bg_used_dirs_count));

		lseek(fd, offset + 18, SEEK_SET);
		rwbytes = read(fd, &(ext2->gd_table.table[count].bg_pad), sizeof(ext2->gd_table.table[count].bg_pad));

		lseek(fd, offset + 20, SEEK_SET);
		rwbytes = read(fd, &(ext2->gd_table.table[count].bg_reserved), sizeof(ext2->gd_table.table[count].bg_reserved));

		// Read block bitmap
		ext2->gd_table.block_bitmap = malloc(sizeof(uint8_t) * ext2->s_blocks_per_group / 8);
		rwbytes = read(fd, (ext2->gd_table.block_bitmap), sizeof(uint8_t) * ext2->s_blocks_per_group / 8);

		// Read inode bitmap
		ext2->gd_table.inode_bitmap = malloc(sizeof(uint8_t) * ext2->s_inodes_per_group / 8);
		lseek(fd, ext2_blockoffset(*ext2, ext2->gd_table.table[count].bg_inode_bitmap), SEEK_SET);
		read(fd, (ext2->gd_table.inode_bitmap), sizeof(uint8_t) * ext2->s_inodes_per_group / 8);

		offset = offset + 32;
		count ++;

	}

}

int ext2_isext2(ext2_t ext2)
{
	if (ext2.s_magic == 0xEF53) {
		return 0;
	} else {
		return 1;
	}
}

ext2_inodeloc_t ext2_find_inode(ext2_t ext2, unsigned int inode)
{
	ext2_inodeloc_t loc;
	loc.block_group = ceil((inode - 1) / ext2.s_inodes_per_group);
	loc.index = (inode - 1) % ext2.s_inodes_per_group;
	loc.containing_block = (loc.index * ext2.s_indoe_size) / ext2.c_block_size;

	return loc;
}

void ext2_free(ext2_t ext2)
{
	free(ext2.gd_table.table);
	free(ext2.gd_table.block_bitmap);
	free(ext2.gd_table.inode_bitmap);

}

int ext2_search_file_entry(int fd, ext2_t ext2, char *file, ext2_diren_t **direns)
{
	ext2_inode_t inode;
	uint32_t i;
	ext2_diren_t *diren = NULL;
	int size = 0;

	i = 1;
	while (i < ext2.s_inodes_per_group + 1) {
		if (getbit(ext2.gd_table.inode_bitmap, i - 1) && i != 1 && i != 3 && i != 4 && i != 5 && i != 6) {
			ext2_read_inode(fd, ext2, i, &inode);
			//printf("inode size: %d, links count: %d\n", inode.i_size, inode.i_links_count);
			if ((inode.i_mode & EXT2_S_IFDIR) == EXT2_S_IFDIR) {
				ext2_read_direns_for_file(fd, ext2, &inode, i, &diren, file);
				if (diren != NULL) {
					if (getbit(ext2.gd_table.inode_bitmap, diren->inode - 1)) {
						*direns = realloc(*direns, sizeof(ext2_diren_t) * (size + 1));
						((*direns)[size]).inode = diren->inode;
						((*direns)[size]).rec_len = diren->rec_len;
						((*direns)[size]).name_len = diren->name_len;
						((*direns)[size]).file_type = diren->file_type;
						strncpy(((*direns)[size]).name, diren->name, 255);
						size++;
					}
					free(diren);
					diren = NULL;
				}
			}
		}
		i++;
	}
	return size;

}

void ext2_read_direns_for_file(int fd, ext2_t ext2, ext2_inode_t *inode, uint32_t inode_no, ext2_diren_t **diren, char *file)
{
	uint32_t offset, block_addr, computed_size, read_addr, read_addr2, read_addr3, block_end;
	int block;
	ext2_diren_t aux_diren;
	inode_no = inode_no;

	for (block = 0; block < 15; block++)
	{
		computed_size = 0;

		if (block < 12) {
			block_addr = inode->i_block[block];
			if (block_addr == 0) {
				// if the block address is zero (it means that this block is not used), compute next block
				continue;
			}
			offset = ext2_blockoffset(ext2, block_addr);
			block_end = 0;
			//printf("block size: %u\n", ext2.c_block_size);
			// while size is less than a block size, read direns in this block
			while (computed_size < ext2.c_block_size) {
				lseek(fd, offset, SEEK_SET);
				read(fd, &aux_diren, sizeof(aux_diren));
				offset = aux_diren.rec_len + offset;
				computed_size = aux_diren.rec_len + computed_size;
				if (aux_diren.name_len != 255) aux_diren.name[aux_diren.name_len] = '\0';
				//printf("diren.inode: %u, diren.name: %s, diren.name_len: %u, size: %u, diren.rec_len: %u, diren.file_type: %u\n", aux_diren.inode, aux_diren.name, aux_diren.name_len, computed_size, aux_diren.rec_len, aux_diren.file_type);
				//printf("inode size: %u\n", inode->i_size);
				//if (computed_size == 65535) sleep(3);
				if ((aux_diren.file_type == EXT2_FT_REG_FILE || aux_diren.file_type == EXT2_FT_DIR) && strncmp(aux_diren.name, file, 255) == 0) {
					// file found
					*diren = malloc(sizeof(ext2_diren_t));
					(*diren)->inode = aux_diren.inode;
					(*diren)->rec_len = aux_diren.rec_len;
					(*diren)->name_len = aux_diren.name_len;
					(*diren)->file_type = aux_diren.file_type;
					strncpy((*diren)->name, aux_diren.name, 255);
					//printf("FILE FOUND! diren == NULL: %d, (*diren)->name: %s\n", diren == NULL, (*diren)->name);
					return;
				}
			}
		}
		if (block == 12) {
			uint32_t din_addr[ext2.c_block_size / sizeof(uint32_t)];
			block_addr = inode->i_block[block];
			if (block_addr == 0) {
				//printf("Block addr is 0\n");
				// if the block address is zero (it means that this block is not used), compute next block
				continue;
			}
			offset = ext2_blockoffset(ext2, block_addr);
			lseek(fd, offset, SEEK_SET);
			read(fd, din_addr, sizeof(din_addr));
			// For each address
			read_addr = 0;
			for (read_addr = 0; read_addr < ext2.c_block_size / sizeof(uint32_t); read_addr++) {
				block_addr = din_addr[read_addr];
				if (block_addr == 0) {
					// if the block address is zero (it means that this block is not used), compute next block
					continue;
				}

				offset = ext2_blockoffset(ext2, block_addr);
				block_end = 0;
				computed_size = 0;
				// while size is less than a block size, read direns in this block
				while (computed_size < ext2.c_block_size) {
					lseek(fd, offset, SEEK_SET);
					read(fd, &aux_diren, sizeof(aux_diren));
					offset = aux_diren.rec_len + offset;
					computed_size = aux_diren.rec_len + computed_size;
					if (aux_diren.name_len != 255) aux_diren.name[aux_diren.name_len] = '\0';
					//printf("diren.inode: %u, diren.name: %s, diren.name_len: %u, size: %u, diren.rec_len: %u, diren.file_type: %u\n", aux_diren.inode, aux_diren.name, aux_diren.name_len, computed_size, aux_diren.rec_len, aux_diren.file_type);

					if ((aux_diren.file_type == EXT2_FT_REG_FILE || aux_diren.file_type == EXT2_FT_DIR) && strncmp(aux_diren.name, file, 255) == 0) {
						// file found
						*diren = malloc(sizeof(ext2_diren_t));
						(*diren)->inode = aux_diren.inode;
						(*diren)->rec_len = aux_diren.rec_len;
						(*diren)->name_len = aux_diren.name_len;
						(*diren)->file_type = aux_diren.file_type;
						strncpy((*diren)->name, aux_diren.name, 255);
						//printf("FILE FOUND! diren == NULL: %d, (*diren)->name: %s\n", diren == NULL, (*diren)->name);
						return;
					}
				}

			}

		}
		if (block == 13) {
			uint32_t tin_addr[ext2.c_block_size / sizeof(uint32_t)];
			block_addr = inode->i_block[block];
			if (block_addr == 0) {
				// if the block address is zero (it means that this block is not used), compute next block
				continue;
			}
			offset = ext2_blockoffset(ext2, block_addr);
			lseek(fd, offset, SEEK_SET);
			read(fd, tin_addr, sizeof(tin_addr));
			// For each address
			read_addr = 0;
			for (read_addr = 0; read_addr < ext2.c_block_size / sizeof(uint32_t); read_addr++) {
				block_addr = tin_addr[read_addr];
				if (block_addr == 0) {
					// if the block address is zero (it means that this block is not used), compute next block
					continue;
				}


				uint32_t din_addr[ext2.c_block_size / sizeof(uint32_t)];

				if (block_addr == 0) {
					// if the block address is zero (it means that this block is not used), compute next block
					continue;
				}
				offset = ext2_blockoffset(ext2, block_addr);
				lseek(fd, offset, SEEK_SET);
				read(fd, din_addr, sizeof(din_addr));
				// For each address
				read_addr2 = 0;
				for (read_addr2 = 0; read_addr2 < ext2.c_block_size / sizeof(uint32_t); read_addr2++) {
					block_addr = din_addr[read_addr2];
					if (block_addr == 0) {
						// if the block address is zero (it means that this block is not used), compute next block
						continue;
					}
					offset = ext2_blockoffset(ext2, block_addr);
					block_end = 0;
					computed_size = 0;
					// while size is less than a block size, read direns in this block
					while (computed_size < ext2.c_block_size) {
						lseek(fd, offset, SEEK_SET);
						read(fd, &aux_diren, sizeof(aux_diren));
						offset = aux_diren.rec_len + offset;
						computed_size = aux_diren.rec_len + computed_size;
						if (aux_diren.name_len != 255) aux_diren.name[aux_diren.name_len] = '\0';
						//printf("diren.inode: %u, diren.name: %s, diren.name_len: %u, size: %u, diren.rec_len: %u, diren.file_type: %u\n", aux_diren.inode, aux_diren.name, aux_diren.name_len, 1, aux_diren.rec_len, aux_diren.file_type);

						if ((aux_diren.file_type == EXT2_FT_REG_FILE || aux_diren.file_type == EXT2_FT_DIR) && strncmp(aux_diren.name, file, 255) == 0) {
							// file found
							*diren = malloc(sizeof(ext2_diren_t));
							(*diren)->inode = aux_diren.inode;
							(*diren)->rec_len = aux_diren.rec_len;
							(*diren)->name_len = aux_diren.name_len;
							(*diren)->file_type = aux_diren.file_type;
							strncpy((*diren)->name, aux_diren.name, 255);
							//printf("FILE FOUND! diren == NULL: %d, (*diren)->name: %s\n", diren == NULL, (*diren)->name);
							return;
						}
					}

				}
			}

		}

		if (block == 14) {
			uint32_t tin_addr[ext2.c_block_size / sizeof(uint32_t)];
			block_addr = inode->i_block[block];
			if (block_addr == 0) {
				// if the block address is zero (it means that this block is not used), compute next block
				continue;
			}
			offset = ext2_blockoffset(ext2, block_addr);
			lseek(fd, offset, SEEK_SET);
			read(fd, tin_addr, sizeof(tin_addr));
			// For each address
			read_addr = 0;
			for (read_addr = 0; read_addr < ext2.c_block_size / sizeof(uint32_t); read_addr++) {
				block_addr = tin_addr[read_addr];
				if (block_addr == 0) {
					// if the block address is zero (it means that this block is not used), compute next block
					continue;
				}


				uint32_t din_addr[ext2.c_block_size / sizeof(uint32_t)];

				if (block_addr == 0) {
					// if the block address is zero (it means that this block is not used), compute next block
					continue;
				}
				offset = ext2_blockoffset(ext2, block_addr);
				lseek(fd, offset, SEEK_SET);
				read(fd, din_addr, sizeof(din_addr));
				// For each address
				read_addr2 = 0;
				for (read_addr2 = 0; read_addr2 < ext2.c_block_size / sizeof(uint32_t); read_addr2++) {
					block_addr = din_addr[read_addr2];
					if (block_addr == 0) {
						// if the block address is zero (it means that this block is not used), compute next block
						continue;
					}

					uint32_t sin_addr[ext2.c_block_size / sizeof(uint32_t)];

					if (block_addr == 0) {
						// if the block address is zero (it means that this block is not used), compute next block
						continue;
					}
					offset = ext2_blockoffset(ext2, block_addr);
					lseek(fd, offset, SEEK_SET);
					read(fd, sin_addr, sizeof(din_addr));
					// For each address
					read_addr3 = 0;
					for (read_addr3 = 0; read_addr3 < ext2.c_block_size / sizeof(uint32_t); read_addr3++) {
						block_addr = sin_addr[read_addr3];
						if (block_addr == 0) {
							// if the block address is zero (it means that this block is not used), compute next block
							continue;
						}

						offset = ext2_blockoffset(ext2, block_addr);
						block_end = 0;
						computed_size = 0;
						// while size is less than a block size, read direns in this block
						while (computed_size != ext2.c_block_size) {
							lseek(fd, offset, SEEK_SET);
							read(fd, &aux_diren, sizeof(aux_diren));
							offset = aux_diren.rec_len + offset;
							computed_size = aux_diren.rec_len + computed_size;
							if (aux_diren.name_len != 255) aux_diren.name[aux_diren.name_len] = '\0';
							//printf("diren.inode: %u, diren.name: %s, diren.name_len: %u, size: %u, diren.rec_len: %u, diren.file_type: %u\n", aux_diren.inode, aux_diren.name, aux_diren.name_len, 1, aux_diren.rec_len, aux_diren.file_type);
							if ((aux_diren.file_type == EXT2_FT_REG_FILE || aux_diren.file_type == EXT2_FT_DIR) && strncmp(aux_diren.name, file, 255) == 0) {
								// file found
								*diren = malloc(sizeof(ext2_diren_t));
								(*diren)->inode = aux_diren.inode;
								(*diren)->rec_len = aux_diren.rec_len;
								(*diren)->name_len = aux_diren.name_len;
								(*diren)->file_type = aux_diren.file_type;
								strncpy((*diren)->name, aux_diren.name, 255);
								//printf("FILE FOUND! diren == NULL: %d, (*diren)->name: %s\n", diren == NULL, (*diren)->name);
								return;
							}
						}
					}
				}

			}
		}
	}

}

int ext2_cat_inode(void *arg, block_reader_context_t cntx)
{
	arg = arg;
	if (cntx.accum_read_size > cntx.inode->i_size) {
		uint32_t print = cntx.inode->i_size % cntx.ext2.c_block_size;
		write(1, cntx.block_content, print);
		return 0;
	} else {
		write(1, cntx.block_content, cntx.ext2.c_block_size);
		return 1;
	}

	return 0;
}

void ext2_read_inode(int fd, ext2_t ext2, int inode_no, ext2_inode_t *inode)
{
	ext2_inodeloc_t inodeloc;

	inodeloc = ext2_find_inode(ext2, inode_no);

	lseek(
	    fd,
	    ext2.gd_table.table[inodeloc.block_group].bg_inode_table * ext2.c_block_size + inodeloc.index * sizeof(ext2_inode_t),
	    SEEK_SET
	);
	lseek(
	    fd,
	    ext2_blockoffset(ext2, ext2.gd_table.table[inodeloc.block_group].bg_inode_table) + inodeloc.index * sizeof(ext2_inode_t),
	    SEEK_SET
	);

	read(fd, inode, sizeof(ext2_inode_t));

}

int ext2_block_reader(int fd, ext2_t ext2, int inode_no, ext2_inode_t *inode, int (*callback)(void*, block_reader_context_t), void* arg)
{

	assert(inode != NULL);

	uint32_t bytes;
	int res;

	block_reader_context_t cntx = {
		.offset = 0,
		.block = 0,
		.block_addr = 0,
		.accum_read_size = 0,
		.read_addr = {0, 0, 0, 0},
		.depth = 0,
		.block_content = NULL,
		.block_size = ext2.c_block_size,
		.read_block_count = 0,

		.fd = fd,
		.ext2 = ext2,
		.inode_no = inode_no,
		.inode = inode
	};


	cntx.block_content = malloc(sizeof(uint8_t) * cntx.block_size);

	assert(cntx.block_content != NULL);


	for (cntx.block = 0; cntx.block < 15; cntx.block++) {

		// Direct blocks
		if (cntx.block < 12) {

			cntx.depth = 0;

			cntx.block_addr = inode->i_block[cntx.block];

			if (cntx.block_addr == 0) {
				// if the block address is zero (it means that this block is not used), compute next block
				continue;
			}
			cntx.read_addr[cntx.depth] = cntx.block_addr;

			cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
			bytes = lseek(fd, cntx.offset, SEEK_SET);
			assert(bytes == cntx.offset);
			cntx.accum_read_size += read(fd, cntx.block_content, cntx.block_size);

			res = callback(arg, cntx);
			cntx.read_block_count++;

			switch (res) {
			case 0:
			{
				return res;
			}
			break;
			case 1:
			{

			}
			default:
			{

			}

			}
		}


		// Single-indirect block
		if (cntx.block == 12) {

			cntx.depth = 0;

			uint32_t si_addr[ext2.c_block_size / sizeof(uint32_t)];
			cntx.block_addr = inode->i_block[cntx.block];
			if (cntx.block_addr == 0) {
				// if the block address is zero (it means that this block is not used), compute next block
				continue;
			}
			cntx.read_addr[cntx.depth] = cntx.block_addr;

			cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
			bytes = lseek(fd, cntx.offset, SEEK_SET);
			assert(bytes == cntx.offset);
			bytes = read(fd, si_addr, sizeof(si_addr));
			assert(bytes == sizeof(si_addr));

			cntx.depth = 1;
			// For each address
			cntx.read_addr[cntx.depth] = 0;
			for (cntx.read_addr[cntx.depth] = 0; cntx.read_addr[cntx.depth] < ext2.c_block_size / sizeof(uint32_t); cntx.read_addr[cntx.depth]++) {
				cntx.block_addr = si_addr[cntx.read_addr[cntx.depth]];
				if (cntx.block_addr == 0) {
					//printf("Address is zero\n");
					// if the block address is zero (it means that this block is not used), compute next block
					continue;
				}
				cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
				// while size is less than a block size, read direns in this block
				bytes = lseek(fd, cntx.offset, SEEK_SET);
				assert(bytes == cntx.offset);

				cntx.accum_read_size += read(fd, cntx.block_content, cntx.block_size);

				res = callback(arg, cntx);
				cntx.read_block_count++;

				switch (res) {
				case 0:
				{
					return res;
				}
				break;
				case 1:
				{

				}
				default:
				{

				}

				}

			}

		}

		// Double-indirect block
		if (cntx.block == 13) {

			cntx.depth = 0;

			uint32_t si_addr[ext2.c_block_size / sizeof(uint32_t)];
			cntx.block_addr = inode->i_block[cntx.block];
			if (cntx.block_addr == 0) {
				//printf("Address is zero\n");
				// if the block address is zero (it means that this block is not used), compute next block
				continue;
			}
			cntx.read_addr[cntx.depth] = cntx.block_addr;

			cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
			bytes = lseek(fd, cntx.offset, SEEK_SET);
			assert(bytes == cntx.offset);
			bytes = read(fd, si_addr, sizeof(si_addr));
			assert(bytes == sizeof(si_addr));

			cntx.depth = 1;
			// For each address
			cntx.read_addr[cntx.depth] = 0;
			for (cntx.read_addr[cntx.depth] = 0; cntx.read_addr[cntx.depth] < ext2.c_block_size / sizeof(uint32_t); cntx.read_addr[cntx.depth]++) {
				cntx.block_addr = si_addr[cntx.read_addr[cntx.depth]];
				if (cntx.block_addr == 0) {
					//printf("Address is zero\n");
					// if the block address is zero (it means that this block is not used), compute next block
					continue;
				}

				uint32_t di_addr[ext2.c_block_size / sizeof(uint32_t)];

				cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
				bytes = lseek(fd, cntx.offset, SEEK_SET);
				assert(bytes = cntx.offset);
				bytes = read(fd, di_addr, sizeof(di_addr));
				assert(bytes == sizeof(di_addr));

				cntx.depth = 2;
				// For each address
				cntx.read_addr[cntx.depth] = 0;
				for (cntx.read_addr[cntx.depth] = 0; cntx.read_addr[cntx.depth] < ext2.c_block_size / sizeof(uint32_t); cntx.read_addr[cntx.depth]++) {
					cntx.block_addr = di_addr[cntx.read_addr[cntx.depth]];
					if (cntx.block_addr == 0) {
						// if the block address is zero (it means that this block is not used), compute next block
						continue;
					}
					cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
					// while size is less than a block size, read direns in this block
					bytes = lseek(fd, cntx.offset, SEEK_SET);
					assert(bytes == cntx.offset);

					cntx.accum_read_size += read(fd, cntx.block_content, cntx.block_size);
					res = callback(arg, cntx);
					cntx.read_block_count++;

					switch (res) {
					case 0:
					{
						return res;
					}
					break;
					case 1:
					{

					}
					default:
					{

					}

					}
					cntx.depth = 2;

				}
				cntx.depth = 1;
			}

		}

		// Triple-indirect block
		if (cntx.block == 14) {

			cntx.depth = 0;
			uint32_t si_addr[ext2.c_block_size / sizeof(uint32_t)];
			cntx.block_addr = inode->i_block[cntx.block];
			if (cntx.block_addr == 0) {
				// if the block address is zero (it means that this block is not used), compute next block
				continue;
			}
			cntx.read_addr[cntx.depth] = cntx.block_addr;

			cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
			bytes = lseek(fd, cntx.offset, SEEK_SET);
			assert(bytes == cntx.offset);
			bytes = read(fd, si_addr, sizeof(si_addr));
			assert(bytes == sizeof(si_addr));

			cntx.depth = 1;

			// For each address
			cntx.read_addr[cntx.depth] = 0;
			for (cntx.read_addr[cntx.depth] = 0; cntx.read_addr[cntx.depth] < ext2.c_block_size / sizeof(uint32_t); cntx.read_addr[cntx.depth]++) {
				cntx.block_addr = si_addr[cntx.read_addr[cntx.depth]];
				if (cntx.block_addr == 0) {
					// if the block address is zero (it means that this block is not used), compute next block
					continue;
				}

				uint32_t di_addr[ext2.c_block_size / sizeof(uint32_t)];

				cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
				bytes = lseek(fd, cntx.offset, SEEK_SET);
				assert(bytes = cntx.offset);
				bytes = read(fd, di_addr, sizeof(di_addr));
				assert(bytes == sizeof(di_addr));

				cntx.depth = 2;
				// For each address
				cntx.read_addr[cntx.depth] = 0;
				for (cntx.read_addr[cntx.depth] = 0; cntx.read_addr[cntx.depth] < ext2.c_block_size / sizeof(uint32_t); cntx.read_addr[cntx.depth]++) {
					cntx.block_addr = di_addr[cntx.read_addr[cntx.depth]];
					if (cntx.block_addr == 0) {
						// if the block address is zero (it means that this block is not used), compute next block
						continue;
					}

					uint32_t ti_addr[ext2.c_block_size / sizeof(uint32_t)];

					cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
					bytes = lseek(fd, cntx.offset, SEEK_SET);
					assert(bytes = cntx.offset);
					bytes = read(fd, ti_addr, sizeof(di_addr));
					assert(bytes == sizeof(di_addr));

					cntx.depth = 3;
					// For each address
					cntx.read_addr[cntx.depth] = 0;
					for (cntx.read_addr[cntx.depth] = 0; cntx.read_addr[cntx.depth] < ext2.c_block_size / sizeof(uint32_t); cntx.read_addr[cntx.depth]++) {
						cntx.block_addr = di_addr[cntx.read_addr[cntx.depth]];
						if (cntx.block_addr == 0) {
							// if the block address is zero (it means that this block is not used), compute next block
							continue;
						}
						cntx.offset = ext2_blockoffset(ext2, cntx.block_addr);
						// while size is less than a block size, read direns in this block
						bytes = lseek(fd, cntx.offset, SEEK_SET);
						assert(bytes == cntx.offset);

						cntx.accum_read_size += read(fd, cntx.block_content, cntx.block_size);


						res = callback(arg, cntx);
						cntx.read_block_count++;

						switch (res) {
						case 0:
						{
							return res;
						}
						break;
						case 1:
						{

						}
						default:
						{

						}

						}
						cntx.depth = 3;
					}
					cntx.depth = 2;
				}
				cntx.depth = 1;
			}
		}
	}

	return res;
}


