#ifndef __EXT2_T_H__
#define __EXT2_T_H__

#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include "../../endian/endian.h"

/**
 * File system specific macros and data structures
 */

#define EXT2_BASEOFFSET(x) 		(1024 + x)

#define EXT2_BLOCKOFFSET(block, block_size) (EXT2_BASEOFFSET(0) + (block-1)*block_size)

#define EXT2_S_IFSOCK		0xC000

#define EXT2_S_IFLNK		0xA000

#define EXT2_S_IFREG		0x8000

#define EXT2_S_IFBLK		0x6000

#define EXT2_S_IFDIR		0x4000

#define EXT2_S_IFCHR		0x2000

#define EXT2_S_IFIFO		0x1000

#define EXT2_S_ISUID		0x0800

#define EXT2_S_ISGID		0x0400

#define EXT2_S_ISVTX		0x0200

#define EXT2_S_IRUSR		0x0100

#define EXT2_S_IWUSR		0x0200

#define EXT2_S_IXUSR		0x0100

#define EXT2_S_IRGRP		0x0080

#define EXT2_S_IWGRP		0x0040

#define EXT2_S_IXGRP		0x0020

#define EXT2_S_IROTH		0x0010

#define EXT2_S_IWOTH		0x0002

#define EXT2_S_IXOTH		0x0001

#define EXT2_FT_UNKNOWN		0

#define EXT2_FT_REG_FILE	1

#define EXT2_FT_DIR			2

#define EXT2_FT_CHRDEV		3

#define EXT2_FT_BLKDEV		4

#define EXT2_FT_FIFO		5

#define EXT2_FT_SOCK		6

#define EXT2_FT_SYMLYNK		7

typedef struct {
	uint32_t	inode;
	uint16_t 	rec_len;
	uint8_t		name_len;
	uint8_t 	file_type;
	uint8_t 	name[255];
} ext2_diren_t;

typedef struct {
	uint16_t	i_mode;
	uint16_t	i_uid;
	uint32_t 	i_size;
	uint32_t	i_atime;
	uint32_t	i_ctime;
	uint32_t	i_mtime;
	uint32_t	i_dtime;
	uint16_t	i_gid;
	uint16_t	i_links_count;
	uint32_t	i_blocks;
	uint32_t	i_flags;
	uint32_t	i_osd1;
	uint32_t	i_block[15];
	uint32_t	i_generation;
	uint32_t	i_fle_acl;
	uint32_t	i_dir_acl;
	uint32_t	i_faddr;
	uint8_t		i_osd2[12];
} ext2_inode_t;

typedef struct {
	unsigned int block_group;
	unsigned int index;
	unsigned int containing_block;
} ext2_inodeloc_t;

typedef struct {
	char		used 	:1;
} ext2_block_t;

// Do not modify this struct!!
typedef struct {
	uint32_t	bg_block_bitmap;
	uint32_t	bg_inode_bitmap;
	uint32_t	bg_inode_table;
	uint16_t	bg_free_blocks_count;
	uint16_t	bg_free_inodes_count;
	uint16_t	bg_used_dirs_count;
	uint16_t	bg_pad;
	uint8_t		bg_reserved[12];
} ext2_groupdesc_t;

typedef struct {
	ext2_groupdesc_t 	*table;
	char				*table_bitmap;
	unsigned int 		table_size;

	uint8_t				*block_bitmap;
	uint8_t				*inode_bitmap;
} ext2_gdtable_t;

typedef struct {
	uint16_t 					s_magic;					// Magic Number
	uint16_t 					s_indoe_size;				// Inode Size
	uint32_t 					s_inodes_count;				// Total Inodes
	uint32_t 					s_first_inode;				// First Inode
	uint32_t 					s_inodes_per_group;			// Inodes per group
	uint32_t 					s_free_inodes_count;		// Free inodes

	uint32_t 					s_log_block_size;			// Block size = 1024 << s_log_block_size
	uint32_t 					s_r_blocks_count;			// Reserved Blocks
	uint32_t 					s_free_blocks_count;		// Free Blocks
	uint32_t 					s_blocks_count;				// Total Blocks
	uint32_t 					s_first_data_block;			// Total Blocks
	uint32_t 					s_blocks_per_group;			// Blocks per group
	uint32_t 					s_frags_per_group;			// Frags per group

	uint8_t 					s_volume_name[16];			// Volume name
	uint8_t 					s_last_mounted[64];			// Last mounted
	time_t						s_lastcheck;				// Last check
	time_t						s_mtime;					// Last mount
	time_t						s_wtime;					// Last written

	uint32_t					c_block_size;				// Computed block size
	ext2_gdtable_t				gd_table;					// Group Descriptor Table
	float 						c_block_group_count;		// Computed number of block groups
} ext2_t;

/**
 * BLOCK READER
 */

/**
 * Block reader context
 */

typedef struct _block_reader_context_t {
	uint32_t offset;
	uint32_t block;				// current block [0, 14]
	uint32_t block_addr;		// current read block address, it must be the same as one of the read_addr
	uint32_t accum_read_size;	// accumulated read bytes
	uint32_t read_addr[4];		// read address for single, double or triple indirect block
	uint32_t depth;				// indirect depth, 0 means no indirection, indirections from [1, 3]
	uint8_t *block_content;		// block content
	uint32_t block_size;		// block size
	uint32_t read_block_count;	// read block count

	int fd;
	ext2_t ext2;
	int inode_no;
	ext2_inode_t *inode;

} block_reader_context_t;

/**
 * Reads the blocks of an inode, calling callback for each block.
 * Callback must return 0 to stop or 1 to continue
 */
int ext2_block_reader(int fd, ext2_t ext2, int inode_no, ext2_inode_t *inode, int (*callback)(void*, block_reader_context_t), void* arg);

/**
 * BLOCK READER END
 */

uint32_t ext2_blockoffset(ext2_t ext2, uint32_t block);

uint32_t ext2_baseoffset(ext2_t ext2, uint32_t offset);

/**
 * [ext2_isext2 description]
 * @param  fd [description]
 * @return    0 if ext2, -1 otherwise
 */
int ext2_isext2(ext2_t ext2);

void ext2_readfs(int fd, ext2_t *ext2);

void ext2_print(ext2_t ext2);

void ext2_print_gdtables(ext2_t ext2);

ext2_inodeloc_t ext2_find_inode(ext2_t ext2, unsigned int inode);

void ext2_read_inode(int fd, ext2_t ext2, int inode_no, ext2_inode_t *inode);

int ext2_search_file_entry(int fd, ext2_t ext2, char *file, ext2_diren_t **direns);

int ext2_cat_inode(void *arg, block_reader_context_t cntx);

void ext2_free(ext2_t ext2);



#endif
