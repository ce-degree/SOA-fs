#ifndef __FS_H__
#define __FS_H__

#include <string.h>
#include <stdlib.h>
#include <regex.h>
#include <ctype.h>
#include <time.h>


#include "./fat16/fat16.h"
#include "./ext2/ext2.h"
#include "../regex/regex.h"
#include "../endian/endian.h"

#define FAT16		0x01
#define EXT2		0x02

typedef struct ext2_fat16_info_t
{
	int ext2_fd;
	ext2_t ext2;
	ext2_inode_t *inode;
	uint32_t inode_no;
	int fat_fd;
	fat16_t *fat16;
	fat16_direntry_t *fat_entry;
	uint32_t prev_cluster;
	uint32_t current_cluster;
} ext2_fat16_info_t;

void fat16_write_clus(int fat_fd, fat16_t *fat16, uint8_t *block, uint32_t block_size, uint16_t cluster_num);

void fat16_create_entry_from_inode(fat16_t fat16, fat16_direntry_t *entry, ext2_inode_t inode, ext2_diren_t diren);

void ext2_copy_inode(int fd, ext2_t ext2, ext2_inode_t *inode, ext2_diren_t diren, int fat_fd, fat16_t *fat16, uint16_t cluster_num, fat16_direntry_t *fat_entry);

void strupper(char *s);

#endif
