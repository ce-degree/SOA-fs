#include "./fat16.h"

char illegal_dirname_chars[17] = {0x22, 0x2A, 0x2B, 0x2C, 0x2E, 0x2F, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F, 0x5B, 0x5C, 0x5D, 0x7C};

int fat16_isfat16(fat16_t fat16);

void fat16_readfs(int fd, fat16_t *fat16);

void fat16_print(fat16_t fat16);

void fat16_read_root_dir(int fd, fat16_t *fat16);

void fat16_write_root_dir(int fd, fat16_t *fat16);

uint8_t fat16_entryattr(uint8_t entryattr, uint8_t mask);

uint8_t fat16_is_free_entry(uint8_t dirname[11]);

uint8_t fat16_is_final_entry(uint8_t dirname[11]);

uint8_t fat16_is_valid_entry(uint8_t dirname[11]);

void fat16_string2entry(uint8_t dirname[11]);

void fat16_entry2string(uint8_t dirname[11]);

void fat16_read_fat(int fd, fat16_t *fat16);

fat16_direntry_t *fat16_search_free_entry(int fd, fat16_t fat16);

uint16_t fat16_next_free_cluster(fat16_t *fat16, uint16_t current);

void fat16_mini_cat(int fd, fat16_t fat16, fat16_direntry_response_t entry);

uint32_t fat16_get_free_aunits(fat16_t fat16, uint16_t ***free_aunits);

uint8_t fat16_is_aunit_free(uint16_t aunit);

void fat16_free(fat16_t fat16);

int fat16_isfat16(fat16_t fat16)
{
	if (fat16.BPB_sec_per_clus == 0 || fat16.BPB_bytes_per_sec == 0) {
		return -1;
	}

	unsigned int root_dir_sectors = ((fat16.BPB_root_ent_cnt * 32) + (fat16.BPB_bytes_per_sec - 1)) / fat16.BPB_bytes_per_sec;
	unsigned int data_sec = fat16.BPB_tot_sec_16 - (fat16.BPB_rsvd_sec_cnt + (fat16.BPB_num_FATs * fat16.BPB_FATSz16) + root_dir_sectors);
	unsigned int count_of_clusters = data_sec / fat16.BPB_sec_per_clus;

	if (fat16.BPB_FATSz16 == 0) {
		return -1;
	}

	if (count_of_clusters < 4085) {
		return -1;
	} else if (count_of_clusters < 65525) {
		return 0;
	} else {
		return -1;
	}
}

void fat16_readfs(int fd, fat16_t *fat16)
{
	ssize_t rwbytes = 0;
	int i;

	rwbytes = rwbytes;

	lseek(fd, 3, SEEK_SET);
	rwbytes = read(fd, &fat16->BS_OEM_name, sizeof(fat16->BS_OEM_name));

	lseek(fd, 11, SEEK_SET);
	rwbytes = read(fd, &fat16->BPB_bytes_per_sec, sizeof(fat16->BPB_bytes_per_sec));

	lseek(fd, 13, SEEK_SET);
	rwbytes = read(fd, &fat16->BPB_sec_per_clus, sizeof(fat16->BPB_sec_per_clus));

	lseek(fd, 14, SEEK_SET);
	rwbytes = read(fd, &fat16->BPB_rsvd_sec_cnt, sizeof(fat16->BPB_rsvd_sec_cnt));

	lseek(fd, 16, SEEK_SET);
	rwbytes = read(fd, &fat16->BPB_num_FATs, sizeof(fat16->BPB_num_FATs));

	lseek(fd, 17, SEEK_SET);
	rwbytes = read(fd, &fat16->BPB_root_ent_cnt, sizeof(fat16->BPB_root_ent_cnt));

	lseek(fd, 22, SEEK_SET);
	rwbytes = read(fd, &fat16->BPB_FATSz16, sizeof(fat16->BPB_FATSz16));

	lseek(fd, 43, SEEK_SET);
	rwbytes = read(fd, &fat16->BS_vol_lab, sizeof(fat16->BS_vol_lab));

	i = sizeof(fat16->BS_vol_lab);
	while (fat16->BS_vol_lab[i] == ' ' && i != 0) i--;

	if (i != 11) {
		fat16->BS_vol_lab[i] = '\0';
	}

	lseek(fd, 19, SEEK_SET);
	rwbytes = read(fd, &fat16->BPB_tot_sec_16, sizeof(fat16->BPB_tot_sec_16));
}

void fat16_print(fat16_t fat16)
{

	printf("FileSystem: FAT16\n\n");

	printf("System Name: %s\n", (char *)fat16.BS_OEM_name);
	printf("Sector Size: %d\n", (int)fat16.BPB_bytes_per_sec);
	printf("Sectors per Cluster: %d\n", (int)fat16.BPB_sec_per_clus);
	printf("Reserved Sectors: %d\n", (int)fat16.BPB_rsvd_sec_cnt);
	printf("Number of FATs: %d\n", (int)fat16.BPB_num_FATs);
	printf("Maximum Root Entries: %d\n", (int)fat16.BPB_root_ent_cnt);
	printf("Sectors per FAT: %d\n", (int)fat16.BPB_FATSz16);
	printf("Label: %.*s\n", 11, (char *)fat16.BS_vol_lab);

}

void fat16_read_root_dir(int fd, fat16_t *fat16)
{
	fat16_direntry_t entry;
	ssize_t rwbytes = 0;
	unsigned int offset = 0, count;
	unsigned int first_root_dir_sec_num = 0;

	first_root_dir_sec_num = fat16->BPB_rsvd_sec_cnt + (fat16->BPB_num_FATs * fat16->BPB_FATSz16);
	offset = first_root_dir_sec_num * fat16->BPB_bytes_per_sec + 0;

	fat16->root_entries = malloc(sizeof(entry) * fat16->BPB_root_ent_cnt);

	lseek(fd, offset, SEEK_SET);
	rwbytes = read(fd, fat16->root_entries, sizeof(entry) * fat16->BPB_root_ent_cnt);

	for (count = 0; count < fat16->BPB_root_ent_cnt; count++) {
		entry = fat16->root_entries[count];
		//printf("entry_name: %.*s\n", 11, entry.name);
		// If it is an archive entry...
	}

}

void fat16_write_root_dir(int fd, fat16_t *fat16)
{
	fat16_direntry_t entry;
	ssize_t rwbytes = 0;
	unsigned int offset = 0;
	unsigned int first_root_dir_sec_num = 0;

	first_root_dir_sec_num = fat16->BPB_rsvd_sec_cnt + (fat16->BPB_num_FATs * fat16->BPB_FATSz16);
	offset = first_root_dir_sec_num * fat16->BPB_bytes_per_sec + 0;

	lseek(fd, offset, SEEK_SET);
	rwbytes = write(fd, fat16->root_entries, sizeof(entry) * fat16->BPB_root_ent_cnt);

	if (rwbytes <= 0) {
		perror("The following error ocurred");
	}

	//printf("written\n");

}

void fat16_searchfileentries(fat16_t fat16, fat16_direntry_response_t fileentries[512], unsigned int *size)
{
	char entry_name[11];
	fat16_direntry_t entry;
	unsigned int count = 0, done = 0;
	int i = 0;
	*size = 0;

	for (count = 0; count < fat16.BPB_root_ent_cnt; count++) {
		entry = fat16.root_entries[count];
		//printf("entry_name: %.*s\n", 11, entry.name);
		// If it is an archive entry...
		if (
		    fat16_is_valid_entry(entry.name) == 1 &&
		    !fat16_is_free_entry(entry.name) &&
		    !fat16_is_final_entry(entry.name) &&
		    (
		        (entry.attr & FAT_ATTR_ARCHIVE) == FAT_ATTR_ARCHIVE ||
		        ((entry.attr & FAT_ATTR_DIRECTORY) == FAT_ATTR_DIRECTORY &&
		         (entry.attr & (FAT_ATTR_HIDDEN | FAT_ATTR_SYSTEM | FAT_ATTR_VOLUMEID))) == 0
		    )
		)
		{
			(*size)++;
			fileentries[*size - 1].entry = fat16.root_entries[count];
			strncpy(entry_name, fileentries[*size - 1].entry.name, 11);
			// Set the filename
			i = 7;
			while (entry_name[i] == ' ' && i >= 0) {
				entry_name[i] = '\0';
				i--;
			}
			strncpy(fileentries[*size - 1].filename, entry_name, 8);
			if (fileentries[*size - 1].filename[0] == 0x05) {
				fileentries[*size - 1].filename[0] = 0xE5;
			}
			fileentries[*size - 1].filename[8] = '\0';

			// Set the extension
			fileentries[*size - 1].extension[0] = entry_name[8];
			fileentries[*size - 1].extension[1] = entry_name[9];
			fileentries[*size - 1].extension[2] = entry_name[10];
			i = 2;
			while (fileentries[*size - 1].extension[i] == ' ' && i >= 0) {
				fileentries[*size - 1].extension[i] = '\0';
				i--;
			}
			fileentries[*size - 1].extension[3] = '\0';

		} else if (fat16_is_final_entry(fat16.root_entries[count].name)) {
			// If it is the final entry, stop looping
			done = 1;
		}
	}

}

uint8_t fat16_entryattr(uint8_t entryattr, uint8_t mask)
{
	entryattr = entryattr & 0x3F;
	return entryattr & mask;
}

uint8_t fat16_is_free_entry(uint8_t dirname[11])
{
	if (dirname[0] == 0xE5) {
		return 1;
	} else {
		return 0;
	}
}

uint8_t fat16_is_final_entry(uint8_t dirname[11])
{
	if (dirname[0] == 0x00) {
		return 1;
	} else {
		return 0;
	}
}


uint8_t fat16_is_valid_entry(uint8_t dirname[11])
{
	unsigned int count, count2;
	uint8_t check = 1;

	for (count = 0; count < 11 && check != 0; count++) {
		if (count == 0 && dirname[count] == 0x05) {
			check *= 1;
		} else {
			if (count == 0 && dirname[count] == 0x20) {
				check = 0;
				return 0;
			}
			for (count2 = 0; count2 < sizeof(illegal_dirname_chars); count2 ++) {
				if (dirname[count] == illegal_dirname_chars[count2]) {
					check *= 0;
					return 0;
				}
			}
			if (dirname[count] < 0x20) {
				check *= 0;
				return 0;
			}
		}
	}

	return check;
}


void fat16_entry2string(uint8_t dirname[11])
{
	unsigned int count;
	for (count = 0; count < 11; count++) {
		if (count == 0 && dirname[count] == 0x05) {
			dirname[count] = 0xE5;
		}
	}
}

void fat16_read_fat(int fd, fat16_t *fat16)
{
	uint32_t offset;
	uint32_t i;
	fat16->size = (fat16->BPB_FATSz16 * fat16->BPB_bytes_per_sec) / sizeof(uint16_t);

	fat16->FAT = malloc(sizeof(uint16_t) * fat16->size);

	// That's the start of the first FAT Region

	offset = fat16->BPB_rsvd_sec_cnt * fat16->BPB_bytes_per_sec;

	lseek(fd, offset, SEEK_SET);
	read(fd, fat16->FAT, sizeof(uint16_t) * fat16->size);

	for (i = 0; i < fat16->size; i++) {
		//printf("FAT entry content[%u]: %02X\n", i, fat16->FAT[i]);
	}

	//printf("Fat entry total count: %u\n", fat16->size);

}

void fat16_write_fat(int fd, fat16_t *fat16) {

	ssize_t rwbytes;
	uint32_t offset;
	uint32_t i;
	fat16->size = (fat16->BPB_FATSz16 * fat16->BPB_bytes_per_sec) / sizeof(uint16_t);

	// That's the start of the first FAT Region

	offset = fat16->BPB_rsvd_sec_cnt * fat16->BPB_bytes_per_sec;

	for (i = 0; i < fat16->BPB_num_FATs; i++) {
		lseek(fd, offset + fat16->size * i, SEEK_SET);
		rwbytes = write(fd, fat16->FAT, sizeof(uint16_t) * fat16->size);

		if (rwbytes <= 0) {
			perror("The following error ocurred");
		}
	}
}

fat16_direntry_t *fat16_search_free_entry(int fd, fat16_t fat16)
{


	fd = fd;
	printf("Searching free entry\n");
	fat16_direntry_t entry;

	unsigned int count = 0;

	for (count = 0; count < fat16.BPB_root_ent_cnt; count++) {

		entry = fat16.root_entries[count];

		// If it is an archive entry...
		if (
		    (fat16_is_free_entry(entry.name) == 1 || fat16_is_final_entry(entry.name) == 1)
		)
		{
			return &(fat16.root_entries[count]);
		}
	}

	return NULL;

}

uint16_t fat16_next_free_cluster(fat16_t *fat16, uint16_t current)
{
	for (current = current; current < fat16->size; current++)
	{
		if (1 == fat16_is_aunit_free(fat16->FAT[current]) && current > 2) {
			return current;
		}
	}
	return 0;
}


uint32_t fat16_get_free_aunits(fat16_t fat16, uint16_t ***free_aunits)
{
	uint32_t i;

	uint32_t size = 0;

	//printf("fat16 size: %u\n", fat16.size);

	for (i = 0; i < fat16.size; i++) {
		//printf("volta\n");
		if (1 == fat16_is_aunit_free(fat16.FAT[i])) {
			*free_aunits = realloc(*free_aunits, sizeof(uint16_t *) * (size + 1));
			(*free_aunits)[size] = &(fat16.FAT[i]);
			//printf("FAT free entry content: %02X\n", *((*free_aunits)[size]));
			size++;
		}
	}
	//printf("Free aunit size (inside): %u, pointer: %X\n", size, *(free_aunits));

	return size;
}

void fat16_mini_cat(int fd, fat16_t fat16, fat16_direntry_response_t entry)
{
	unsigned int offset = 0;
	unsigned int first_root_dir_sec_num = 0;

	fd = fd;

	first_root_dir_sec_num = fat16.BPB_rsvd_sec_cnt + (fat16.BPB_num_FATs * fat16.BPB_FATSz16);
	offset = first_root_dir_sec_num * fat16.BPB_bytes_per_sec + sizeof(fat16_direntry_t) * fat16.BPB_root_ent_cnt;
	offset = (offset / fat16.BPB_bytes_per_sec) / fat16.BPB_sec_per_clus;

	printf("FstClusLO: %02X\n", entry.entry.fst_clus_lo);

	offset = offset + entry.entry.fst_clus_lo - 2;

	printf("cluster offset: %u\n", offset);


}

uint8_t fat16_is_aunit_free(uint16_t aunit)
{
	if (aunit == 0x00) {
		return 1;
	}
	return 0;
}

void fat16_free(fat16_t fat16)
{
	free(fat16.root_entries);
}


