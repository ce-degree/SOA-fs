#ifndef __FAT16_T_H__
#define __FAT16_T_H__

#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define FAT_ATTR_READ_ONLY		0x01
#define FAT_ATTR_HIDDEN			0x02
#define FAT_ATTR_SYSTEM			0x04
#define FAT_ATTR_VOLUMEID		0x08
#define FAT_ATTR_DIRECTORY		0x10
#define FAT_ATTR_ARCHIVE		0x20
#define FAT_ATTR_LONG_NAME		FAT_ATTR_READ_ONLY | FAT_ATTR_HIDDEN | FAT_ATTR_SYSTEM | FAT_ATTR_VOLUMEID


typedef struct fat16_direntry_t {
	uint8_t			name[11];
	uint8_t			attr;
	uint8_t			NTRes;
	uint8_t			crt_time_tenth;
	uint16_t		crt_time;
	uint16_t		crt_date;
	uint16_t		lst_acc_date;
	uint16_t 		fst_clus_hi;
	uint16_t		wrt_time;
	uint16_t		wrt_date;
	uint16_t		fst_clus_lo;
	uint32_t		file_size;
} fat16_direntry_t;

typedef struct fat16_direntry_response_t {
	fat16_direntry_t 		entry;
	uint8_t					filename[9];
	uint8_t 				extension[4];
} fat16_direntry_response_t;

typedef struct fat16_t {
	uint16_t			BPB_tot_sec_16;
	uint8_t				BS_OEM_name[8];
	uint16_t			BPB_bytes_per_sec;
	uint8_t				BPB_sec_per_clus;
	uint16_t 			BPB_rsvd_sec_cnt;
	uint8_t 			BPB_num_FATs;
	uint16_t			BPB_root_ent_cnt;
	uint16_t			BPB_FATSz16;
	uint8_t				BS_vol_lab[11];
	fat16_direntry_t	*root_entries;

	uint16_t 			*FAT;
	uint32_t			size;
} fat16_t;

/**
 * [ext2_isext2 description]
 * @param  fd [description]
 * @return    0 if fat16, -1 otherwise
 */
int fat16_isfat16(fat16_t fat16);

void fat16_readfs(int fd, fat16_t *fat16);

void fat16_print(fat16_t fat16);

void fat16_read_root_dir(int fd, fat16_t *fat16);

void fat16_write_root_dir(int fd, fat16_t *fat16);

void fat16_searchfileentries(fat16_t fat16, fat16_direntry_response_t *fileentries, unsigned int *size);

uint8_t fat16_entryattr(uint8_t entryattr, uint8_t mask);

uint8_t fat16_is_free_entry(uint8_t dirname[11]);

uint8_t fat16_is_final_entry(uint8_t dirname[11]);

uint8_t fat16_is_valid_entry(uint8_t dirname[11]);

void fat16_string2entry(uint8_t dirname[11]);

void fat16_entry2string(uint8_t dirname[11]);

void fat16_read_fat(int fd, fat16_t *fat16);

void fat16_write_fat(int fd, fat16_t *fat16);

fat16_direntry_t *fat16_search_free_entry(int fd, fat16_t fat16);

uint16_t fat16_next_free_cluster(fat16_t *fat16, uint16_t current);

uint32_t fat16_get_free_aunits(fat16_t fat16, uint16_t ***free_aunits);

void fat16_mini_cat(int fd, fat16_t fat16, fat16_direntry_response_t entry);

uint8_t fat16_is_aunit_free(uint16_t aunit);

void fat16_free(fat16_t fat16);

#endif
