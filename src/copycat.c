#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>

#include "./shell/shell.h"
#include "./util/fs/fs.h"
#include "./util/fs/ext2/ext2.h"
#include "./util/fs/fat16/fat16.h"

char* buildCommand(int argc, char **argv);

void doInfo(void *fs, int type);

void doFind(int fd, void *fs, int type, char *file);

void doCat(int fd, void *fs, int type, char *file);

void doCopy(int fd1, int fd2, ext2_t *ext2, fat16_t *fat16, char *file);

char char_in_string(char *string, char c);


int main(int argc, char **argv)
{
	ext2_t ext2;
	fat16_t fat16;
	void *fs, *fs2;
	int type;
	char *command, **sh_argv;
	int sh_argc = -1, option, fd1, fd2, count;
	option = -1;

	command = buildCommand(argc, argv);

	option = sh_parseCommand(command, &sh_argc, &sh_argv);
	if ((option == SH_INFO && sh_argc == 1) || (option == SH_FIND && sh_argc == 2) || (option == SH_CAT && sh_argc == 2)) {

		fd1 = open(sh_argv[0], O_RDONLY);

		if (fd1 <= 0) {
			printf("Error opening file\n");
			exit(EXIT_FAILURE);
		}

		ext2_readfs(fd1, &ext2);
		fat16_readfs(fd1, &fat16);
		if (0 == ext2_isext2(ext2)) {

			type = EXT2;
			fs = &ext2;
		} else if (0 == fat16_isfat16(fat16)) {
			type = FAT16;
			fat16_read_root_dir(fd1, &fat16);
			fs = &fat16;
		} else {
			printf("Filesystem not recognized\n");
			goto PROG_END;
		}

	} else if ((option == SH_COPY && sh_argc == 3)) {

		fd1 = open(sh_argv[0], O_RDWR);

		if (fd1 <= 0) {
			printf("Error opening file\n");
			exit(EXIT_FAILURE);
		}

		fd2 = open(sh_argv[1], O_RDWR);

		if (fd2 <= 0) {
			printf("Error opening file\n");
			exit(EXIT_FAILURE);
		}

		ext2_readfs(fd1, &ext2);

		if (0 == ext2_isext2(ext2)) {
			fs = &ext2;
		} else {
			printf("Source filesystem error\n");
			goto PROG_END;

		}

		fat16_readfs(fd2, &fat16);

		if (0 == fat16_isfat16(fat16)) {
			fs2 = &fat16;
			fat16_read_root_dir(fd2, &fat16);
		} else {
			printf("Source filesystem error\n");
			goto PROG_END;

		}

	}
	switch (option) {
	case SH_INFO:
	{
		doInfo(fs, type);
	}
	break;
	case SH_FIND:
	{
		doFind(fd1, fs, type, sh_argv[1]);
	}
	break;
	case SH_CAT:
	{
		if (type == FAT16) {
			printf("Option not implemented for FAT16 filesystems\n");
			goto PROG_END;
		}
		doCat(fd1, fs, type, sh_argv[1]);
	}
	break;
	case SH_COPY:
	{
		doCopy(fd1, fd2, &ext2, &fat16, sh_argv[2]);
	}
	break;
	case -1:
	{
		printf("Invalid option\n");
	}
	break;
	default:
		break;

	}
PROG_END:
	if (sh_argc != -1) {
		for (count = 0; count < sh_argc; count++) {
			free(sh_argv[count]);
		}
		free(sh_argv);
	}

	return EXIT_SUCCESS;
}

void doInfo(void *fs, int type)
{
	ext2_t ext2;
	fat16_t fat16;

	switch (type) {
	case EXT2:
	{
		ext2 = (*(ext2_t *)(fs));
		ext2_print(ext2);
		//printf("\nGroup descriptor tables\n");
		//ext2_print_gdtables(ext2);
		ext2_free(ext2);
	}
	break;
	case FAT16:
	{
		fat16 = (*(fat16_t *)(fs));
		fat16_print(fat16);
	}
	break;
	}

}

void doFind(int fd, void *fs, int type, char *file)
{
	ext2_t ext2;
	fat16_t fat16;
	int i = 0;
	char buff[20];

	printf("File to find: %s\n", file);

	switch (type) {
	case EXT2:
	{

		ext2_diren_t *direns;
		ext2_inode_t inode;
		int size = 0;
		int i = 0;

		ext2 = (*(ext2_t *)(fs));

		direns = malloc(sizeof(ext2_diren_t) * 1);

		size = ext2_search_file_entry(fd, ext2, file, &direns);
		//printf("size: %u\n", size);
		for (i = 0; i < size; i++) {
			if (direns[i].file_type == EXT2_FT_DIR) {
				printf("Directory %.*s found!\n", 255, (direns[i]).name);

			} else if (direns[i].file_type == EXT2_FT_REG_FILE) {
				ext2_read_inode(fd, ext2, (direns[i]).inode, &inode);
				printf("File %.*s found!, its size is: %u\n", 255, (direns[i]).name, inode.i_size);
			}
		}
		if (size == 0) {
			printf("File not found!\n");
		}
		free(direns);
		ext2_free(ext2);
	}
	break;
	case FAT16:
	{
		char found = 0, not_found = 1;
		strupper(file);

		if (strlen(file) > 12) {
			printf("Invalid filename for FAT16 filesystem\n");
			break;
		}
		int size;

		fat16 = (*(fat16_t *)(fs));
		fat16_direntry_response_t fileentries[512];
		fat16_searchfileentries(fat16, fileentries, &size);
		for (i = 0; i < size; i++) {
			//printf("%s.%s\n", fileentries[i].filename, fileentries[i].extension);
			buff[0] = '\0';
			if (char_in_string(file, '.') == 1 && strlen(fileentries[i].extension) != 0) {
				strcat(buff, fileentries[i].filename);
				strcat(buff, ".");
				strcat(buff, fileentries[i].extension);
				if (strcmp(file, buff) == 0) {
					found = 1;
				}
			} else {
				strcat(buff, fileentries[i].filename);
				if (strcmp(file, buff) == 0) {
					found = 1;
				}
			}
			if (found == 1) {
				not_found = 0;
				found = 0;
				if ((fileentries[i].entry.attr & FAT_ATTR_ARCHIVE) == FAT_ATTR_ARCHIVE) {

					printf("File %s found! Its size is: %u\n", buff, fileentries[i].entry.file_size);
					//printf("Creation date: %X\n", fileentries[i].entry.wrt_date);
					//fat16_mini_cat(fd, fat16, fileentries[i]);
				}
				if ((fileentries[i].entry.attr & FAT_ATTR_DIRECTORY) == FAT_ATTR_DIRECTORY) {
					printf("Directory %s found!\n", buff);

				}
			}
		}
		if (not_found == 1) {
			printf("File not found!\n");
		}

		fat16_free(fat16);
	}
	break;
	}

}

void doCat(int fd, void *fs, int type, char *file)
{
	ext2_t ext2;
	printf("File to find: %s\n", file);

	switch (type) {
	case EXT2:
	{

		ext2_diren_t *direns;
		ext2_inode_t inode;
		int size = 0;
		int i = 0;

		ext2 = (*(ext2_t *)(fs));

		direns = malloc(sizeof(ext2_diren_t) * 1);

		size = ext2_search_file_entry(fd, ext2, file, &direns);
		for (i = 0; i < size; i++) {
			if (direns[i].file_type == EXT2_FT_REG_FILE) {
				ext2_read_inode(fd, ext2, (direns[i]).inode, &inode);
				ext2_block_reader(fd, ext2, (direns[i]).inode, &inode, ext2_cat_inode, NULL);
				printf("\n");
				break;
			}
		}
		if (size == 0) {
			printf("File not found!\n");
		}

		free(direns);
		ext2_free(ext2);
	}
	break;
	case FAT16:
	{
		printf("Nothing to do...\n");
	}
	break;
	}

}

void doCopy(int fd1, int fd2, ext2_t *ext2, fat16_t *fat16, char *file)
{

	printf("This is the file: %s\n", file);

	ext2_diren_t *direns;
	ext2_inode_t inode;
	int size = 0;
	int i = 0;
	uint16_t **free_aunits = NULL;
	uint32_t free_aunits_size;


	direns = malloc(sizeof(ext2_diren_t) * 1);

	size = ext2_search_file_entry(fd1, *ext2, file, &direns);
	for (i = 0; i < size; i++) {
		if (direns[i].file_type == EXT2_FT_REG_FILE) {
			ext2_read_inode(fd1, *ext2, (direns[i]).inode, &inode);
			//ext2_cat_inode(fd1, *ext2, &inode, (direns[i]).inode);
			//printf("\n");
			break;
		}
	}
	if (size == 0) {
		printf("File not found!\n");
		return;
	}


	fat16_direntry_t *fat_entry = fat16_search_free_entry(fd2, *fat16);
	if (fat_entry != NULL) {
		printf("Free entry found!\n");
	} else {
		printf("There aren't free entries, exiting!\n");
		exit(-1);
	}

	// Read the FAT
	fat16_read_fat(fd2, fat16);

	if (ext2->c_block_size != (fat16->BPB_sec_per_clus * fat16->BPB_bytes_per_sec)) {
		printf("Block size and cluster size do not match!\n");
		exit(-1);
	}

	free_aunits_size = fat16_get_free_aunits(*fat16, &free_aunits);

	//printf("Free clusters: %u.\n", free_aunits_size);

	if ((free_aunits_size * (fat16->BPB_bytes_per_sec * fat16->BPB_sec_per_clus)) < inode.i_size) {
		printf("Not enough space in de filesystem.\n");
		exit(-1);
	}

	ext2_copy_inode(fd1, *ext2, &inode, direns[i], fd2, fat16, 0, fat_entry);

	printf("Rewriting root direcory.\n");
	fat16_write_root_dir(fd2, fat16);

	printf("Rewriting FAT regions.\n");
	fat16_write_fat(fd2, fat16);

	free(direns);
	ext2_free(*ext2);
}

char char_in_string(char *string, char c)
{
	int len = strlen(string);
	int i = 0;

	for (i = 0; i < len; i++) {
		if (string[i] == c) {
			return 1;
		}
	}
	return 0;
}

char* buildCommand(int argc, char **argv)
{
	char *command;
	int i, glen, llen;

	command = NULL;
	i = 1;
	glen = llen = 0;
	command = malloc(sizeof(char));

	while (i < argc) {
		llen = strlen(argv[i]);
		glen += llen + 1;
		command = realloc(command, (glen) * sizeof(char));
		strcpy(command + (glen - llen - 1), argv[i]);
		command[glen - 1] = ' ';
		i++;
	}
	command[glen - 1] = '\0';

	return command;
}
