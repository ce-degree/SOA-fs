#ifndef __SHELL_H__
#define __SHELL_H__

#include <regex.h>
#include <stdlib.h>

#include <stdio.h>

#include "../util/regex/regex.h"

#define SH_INFO		0x01
#define SH_FIND		0x02
#define SH_CAT		0x03
#define SH_COPY		0x04


int sh_parseCommand(char *command, int *argc, char ***argv);

#endif
