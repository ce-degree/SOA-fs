#include "./shell.h"

int sh_parseCommand(char *command, int *argc, char ***argv);

int sh_parseCommand(char *command, int *argc, char ***argv)
{
	regex_t regex;
	int reti;
	size_t nmatch;
	regmatch_t *pmatch;

	char buff[512];

	printf("command: %s\n", command);

	reti = regcomp(&regex, "^\\/info (.+)$", REG_EXTENDED | REG_ICASE);

	if (reti) return reti;

	nmatch = 2;
	pmatch = malloc(sizeof(regmatch_t) * nmatch);

	reti = regexec(&regex, command, nmatch, pmatch, 0);

	if (!reti)
	{
		regfree(&regex);
		*argc = 1;
		*argv = (char **)malloc(sizeof(char*) * (*argc));

		regex_getRegexString(buff, command, pmatch, 1);
		*argv[0] = strdup(buff);

		return SH_INFO;

	} else {
		free(pmatch);
		regfree(&regex);
	}

	reti = regcomp(&regex, "^\\/find (.+) (.+)$", REG_EXTENDED | REG_ICASE);

	if (reti) return reti;

	nmatch = 3;
	pmatch = malloc(sizeof(regmatch_t) * nmatch);

	reti = regexec(&regex, command, nmatch, pmatch, 0);

	if (!reti)
	{
		regfree(&regex);
		(*argc) = 2;
		(*argv) = (char **)malloc(sizeof(char*) * (*argc));

		regex_getRegexString(buff, command, pmatch, 1);
		(*argv)[0] = strdup(buff);

		regex_getRegexString(buff, command, pmatch, 2);
		(*argv)[1] = strdup(buff);

		return SH_FIND;

	} else {
		free(pmatch);
		regfree(&regex);
	}

	reti = regcomp(&regex, "^\\/cat (.+) (.+)$", REG_EXTENDED | REG_ICASE);

	if (reti) return reti;

	nmatch = 3;
	pmatch = malloc(sizeof(regmatch_t) * nmatch);

	reti = regexec(&regex, command, nmatch, pmatch, 0);

	if (!reti)
	{
		regfree(&regex);
		(*argc) = 2;
		(*argv) = (char **)malloc(sizeof(char*) * (*argc));

		regex_getRegexString(buff, command, pmatch, 1);
		(*argv)[0] = strdup(buff);

		regex_getRegexString(buff, command, pmatch, 2);
		(*argv)[1] = strdup(buff);

		return SH_CAT;

	} else {
		free(pmatch);
		regfree(&regex);
	}

	reti = regcomp(&regex, "^\\/copy (.+) (.+) (.+)$", REG_EXTENDED | REG_ICASE);

	if (reti) return reti;

	nmatch = 4;
	pmatch = malloc(sizeof(regmatch_t) * nmatch);

	reti = regexec(&regex, command, nmatch, pmatch, 0);

	if (!reti)
	{
		regfree(&regex);
		(*argc) = 3;
		(*argv) = (char **)malloc(sizeof(char*) * (*argc));

		regex_getRegexString(buff, command, pmatch, 1);
		(*argv)[0] = strdup(buff);

		regex_getRegexString(buff, command, pmatch, 2);
		(*argv)[1] = strdup(buff);

		regex_getRegexString(buff, command, pmatch, 3);
		(*argv)[2] = strdup(buff);

		return SH_COPY;

	} else {
		free(pmatch);
		regfree(&regex);
	}

	return -1;

}
